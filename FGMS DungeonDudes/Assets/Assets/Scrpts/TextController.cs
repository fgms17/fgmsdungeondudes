﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TextController : MonoBehaviour {

public UnityEvent myEvent;
public UnityEvent myEvent2;
public UnityEvent myEventHack;
public UnityEvent myEventOmWa;
public UnityEvent myEventShift;
public UnityEvent myEventCellEscape;
public UnityEvent myEventSpiderRoom;
public UnityEvent myEventAxeRoom;
public UnityEvent myEventBridgeRoom;



public Text text;
public Color TextColor;
public Color TextColor1;
public Color TextColor2;
public Color TextColor3;

private enum States { cell, explore_0, help_0, youdied_0, jiggledoorhandle_0, bed_0, cellfreedom_0, bonedeath_0, bucketdeath_0, chickendeath_0, lvloneend_0, Spiderroom_0, Axeroom_0, Blankroom_0, Spiderroom_1, Spiderroom_2, Spiderdeath_0, Spiderdeath_1, Axeroom_1, Axeroom_2, Axedeath_0, Axedeath_1, Blankroom_1, Converge_0, lvltwoend_0, pitcross_0, bridgecross_0, lazyescape_0, lazyescape_1, lazydeath_0, noescape, cliffcross_0, exitgate_0, marvel_0, hacker_0, marvel_1, wallcross_0, flood_0, flood_1, walldeath_0, electrickery } 
private States myState;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update ()
	{
		print (myState);
		if (myState == States.cell) {
			state_cell ();
		} else if (myState == States.explore_0) {
			state_explore_0 ();
		} else if (myState == States.help_0) {
			state_help_0 ();
		} else if (myState == States.youdied_0) {
			state_youdied_0 ();
		} else if (myState == States.jiggledoorhandle_0) {
			state_jiggledoorhandle_0 ();
		} else if (myState == States.bed_0) {
			state_bed_0 ();
		} else if (myState == States.bonedeath_0) {
			state_bonedeath_0 ();
		} else if (myState == States.cellfreedom_0) {
			state_cellfreedom_0 ();
		} else if (myState == States.bucketdeath_0) {
			state_bucketdeath_0 ();
		} else if (myState == States.lvloneend_0) {
			state_lvloneend_0 ();
		} else if (myState == States.chickendeath_0) {
			state_chickendeath_0 ();
		} else if (myState == States.Spiderroom_0) {
			state_Spiderroom_0 ();
		} else if (myState == States.Axeroom_0) {
			state_Axeroom_0 ();
		} else if (myState == States.Blankroom_0) {
			state_Blankroom_0 ();
		} else if (myState == States.Axeroom_1) {
			state_Axeroom_1 ();
		} else if (myState == States.Axeroom_2) {
			state_Axeroom_2 ();
		} else if (myState == States.Spiderroom_1) {
			state_Spiderroom_1 ();
		} else if (myState == States.Spiderroom_2) {
			state_Spiderroom_2 ();
		} else if (myState == States.Spiderdeath_0) {
			state_Spiderdeath_0 ();
		} else if (myState == States.Spiderdeath_1) {
			state_Spiderdeath_1 ();
		} else if (myState == States.Axedeath_0) {
			state_Axedeath_0 ();
		} else if (myState == States.Axedeath_1) {
			state_Axedeath_1 ();
		} else if (myState == States.Converge_0) {
			state_Converge_0 ();
		} else if (myState == States.lvltwoend_0) {
			state_lvltwoend_0 ();
		} else if (myState == States.bridgecross_0) {
			state_bridgecross_0 ();
		} else if (myState == States.pitcross_0) {
			state_pitcross_0 ();
		} else if (myState == States.lazyescape_0) {
			state_lazyescape_0 ();
		} else if (myState == States.lazyescape_1) {
			state_lazyescape_1 ();
		} else if (myState == States.lazydeath_0) {
			state_lazydeath_0 ();
		} else if (myState == States.noescape) {
			state_noescape ();
		} else if (myState == States.cliffcross_0) {
			state_cliffcross_0 ();
		} else if (myState == States.exitgate_0) {
			state_exitgate_0 ();
		} else if (myState == States.marvel_0) {
			state_marvel_0 ();
		} else if (myState == States.hacker_0) {
			state_hacker_0 ();
		} else if (myState == States.marvel_1) {
			state_marvel_1 ();
		} else if (myState == States.wallcross_0) {
			state_wallcross_0 ();
		} else if (myState == States.walldeath_0) {
			state_walldeath_0 ();
		} else if (myState == States.flood_0) {
			state_flood_0 ();
		} else if (myState == States.flood_1) {
			state_flood_1 ();
		} else if (myState == States.electrickery) {
			state_electrickery ();
		}
	}


	void state_cell ()
	{
		text.color = TextColor1;
		text.text = "You awake deep in a dungeon cell. You have no memory of who, " +
		"or where, you are. What do you do? \n\n" +
		"Press W to explore, A to call for help, or D to break neck.";
		if (Input.GetKeyDown (KeyCode.W)) {
			myState = States.explore_0;
		}
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.help_0;
			myEventOmWa.Invoke();
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.youdied_0;
			myEvent.Invoke();
		}
		if (Input.GetKeyDown (KeyCode.S)) {
			myState = States.hacker_0;
			myEventHack.Invoke();
		}
	}
		void state_explore_0 ()
	{
		text.color = TextColor3;
		text.text = "You glance around, wondering why you are here. You see a " +
		"locked exit, a bed, and a bucket of...  \n\n" +
		"Press W to jiggle the door handle, A to lay down in the bed, or D to look into the bucket";
		if (Input.GetKeyDown (KeyCode.W)) {
			myState = States.jiggledoorhandle_0;
		}
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.bed_0;
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myEvent.Invoke();
			myState = States.bucketdeath_0;
		}
	}
		void state_help_0 () 
	{
			text.color = TextColor3;
			text.text = "You call out, searching for help. Your voice reverberates around " +
			"the chamber, and you suddenly get the uneasy feeling that something is watching you.  \n\n" +
			"Press W to start exploring";
		if (Input.GetKeyDown (KeyCode.W)) {
			myState = States.explore_0;
		}
	}
		void state_youdied_0 ()
	{
		text.color = TextColor2;
		text.text = "You broke your own neck and died. Do you have no sense of self-preservation? " +
		"Honestly, what an idiot. \n\n" +
		"Press R to restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
		void state_jiggledoorhandle_0 ()
	{
		text.color = TextColor3;
		text.text = "You jiggle the door handle. " +
		"It's locked. \n\n" +
		"Press W to return to exploring";
		if (Input.GetKeyDown (KeyCode.W)) {
			myState = States.explore_0;
		}
	}
			void state_bed_0 ()
	{
		text.color = TextColor3;
		text.text = "As you lay down, you feel something poke you. Reaching down, you " +
		"find a couple of small, sharp bones. \n\n" +
		"Press A to pick the cell lock, or D to throw the bones at the wall";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.cellfreedom_0;
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.bonedeath_0;
			myEvent.Invoke();
		}
	}
			void state_bonedeath_0 ()
	{
		text.color = TextColor2;
		text.text = "The bones bounce off the wall and fly back at you, stabbing you with " +
		"bones all across your chest. Unsurprisingly, you died. \n\n" +
		"Press R to restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
			void state_cellfreedom_0 ()
	{
		text.color = TextColor3;
		text.text = "You pick the lock, and the door swings open. you step out into the unknown,  " +
		"bracing yourself for whatever comes next. \n\n" +
		"Press A to return to your cell, or D to continue your adventure";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.chickendeath_0;
			myEvent.Invoke();
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.lvloneend_0;
			myEventCellEscape.Invoke();
		}
	}
			void state_bucketdeath_0 ()
	{
		text.color = TextColor2;
		text.text = "You peek into the bucket, and realize that this was meant to be a toilet. " +
		"Unfourtunatly, it was too late. The stench overpowered you, and you died. \n\n" +
		"Press R to restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
			void state_chickendeath_0 ()
	{
		text.color = TextColor2;
		text.text = "You look out into the darkness of the dungeon, and decide escaping is too " +
		"much work. you return to your cell, where you starved to death. What a wimp. \n\n" +
		"Press R to restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
			void state_lvloneend_0 ()
	{
		text.color = TextColor3;
		text.text = "You venture onward into the dungeon, and find a large, dim room with three exits. " +
		"The first one has a spider engraved on the door, the second has an axe, and the third is blank. \n\n" +
		"Press A to enter the Spider Door, press W to enter the Axe Door, and press D to enter the Blank Door";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.Spiderroom_0;
			myEventSpiderRoom.Invoke();
		}
		if (Input.GetKeyDown (KeyCode.W)) {
			myState = States.Axeroom_0;
			myEventAxeRoom.Invoke();
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.Blankroom_0;
			myEvent.Invoke();
		}
	}
			void state_Spiderroom_0 ()
	{
		text.color = TextColor3;
		text.text = "You enter the Spider Door. The room is dimly lit, with a human skeleton in one corner, " +
		"and a torch on the wall. Immediately, Giant Spiders begin to crawl through large cracks in the walls. \n\n" +
		"Press A to grab a bone off the skeleton, or press D to grab the torch";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.Spiderroom_1;
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.Spiderdeath_0;
			myEvent.Invoke();
		}
	}
			void state_Spiderroom_1 ()
	{
		text.color = TextColor3;
		text.text = "You swing the bone, scattering spiders left and right. You dash towards the door on the other " +
		"side of the room. when you arrive, you realize that the door is jammed. \n\n" +
		"Press A to sneeze, or press D to kick the door open";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.Spiderroom_2;
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.Spiderdeath_1;
			myEvent.Invoke();
		}
	}
			void state_Spiderroom_2 ()
	{
		text.color = TextColor3;
		text.text = "The mucas from your sneeze loosens the hinges, and the door is opened. " +
		"The opening looms before you. \n\n" +
		"Press W to continue";
		if (Input.GetKeyDown (KeyCode.W)) {
			myState = States.Converge_0;
			myEventBridgeRoom.Invoke();
		}
	}
			void state_Spiderdeath_0 ()
	{
		text.color = TextColor2;
		text.text = "You reach for the torch, but miss, and instead catch your sleeve on fire. " +
		"You burn to death, and the spiders have a good meal. \n\n" +
		"Press R to restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
			void state_Spiderdeath_1 ()
	{
		text.color = TextColor2;
		text.text = "You kick the door and break your foot. " +
		"You die to the spiders, and the spiders are very happy. \n\n" +
		"Press R to restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
			void state_Axeroom_0 ()
	{
		text.color = TextColor3;
		text.text = "You enter the Axe Door. the room is large, with hundreds of Skeletons scattered around the room. " +
		"You take a step forward, and an enormous double-headed axe swings past. you peer down the hall as more and more axes appear, swinging back and forth. \n\n" +
		"Press A to grab a shield off the skeleton, or press D to run for it.";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.Axedeath_0;
			myEvent.Invoke();
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.Axeroom_1;
		}
	}
				void state_Axeroom_1 ()
	{
		text.color = TextColor3;
		text.text = "You begin to run. Somehow, you survive, and reach the door on the other side. (That, or some crazy overlord, who happens to be coding your every move, just couldn't think of a better way to do this). " +
		"Either way, you try to open the door, and realize it's locked. \n\n" +
		"Press A to do that one thing again, or press D to use a skull to smash the lock";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.Axeroom_2;
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.Axedeath_1;
			myEvent.Invoke();
		}
	}
				void state_Axeroom_2 ()
	{
		text.color = TextColor3;
		text.text = "You pick the lock with a couple of bones, and the door opens. " +
		"The opening looms before you. \n\n" +
		"Press W to continue";
		if (Input.GetKeyDown (KeyCode.W)) {
			myState = States.lvltwoend_0;
			myEventBridgeRoom.Invoke();
		}
	}
				void state_Axedeath_0 ()
	{
		text.color = TextColor2;
		text.text = "Turns out small wooden shield is not useful for blocking giant axes. " +
		"Go figure, you died. \n\n" +
		"Press R to restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
				void state_Axedeath_1 ()
	{
		text.color = TextColor2;
		text.text = "You couldn't reach any other skulls, so you used your own. " +
		"You died. Duh. \n\n" +
		"Press R to restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
				void state_Blankroom_0 ()
	{
		text.color = TextColor2;
		text.text = "You stepped through the Blank Door and fell to your death.  " +
		"Apparently, 'blank' means 'no floor'. \n\n" +
		"Press R to restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
				void state_Converge_0 ()
	{
		text.color = TextColor3;
		text.text = "You stepped through the door and entered a long hallway. You realized the other doors both led to this hallway as well.  " +
		"You continued on your path to escape. You were so close! \n\n" +
		"Press W to continue";
		if (Input.GetKeyDown (KeyCode.W)) {
			myState = States.lvltwoend_0;
		}
	}
		void state_lvltwoend_0 ()
	{
		text.color = TextColor3;
		text.text = "You step further into the hall and discover an enormous gate, with a large set of " +
		"stone stairs behind it. Unfourtunately, there was a massive, seemingly bottomless pit between you and the exit \n\n" +
		"Press A to examine the pit, or press D to examine the walls";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.pitcross_0;
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.wallcross_0;
		}
	}
		void state_pitcross_0 ()
	{
		text.color = TextColor3;
		text.text = "You look into the abyss, and consider how to get across. There is a small ledge " +
		"running across the edges of the pit, and a long swinging rope bridge running through the center. \n\n" +
		"Press A to try to cross the bridge, or press D to cross the ledge";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.bridgecross_0;
			myEvent.Invoke();
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.cliffcross_0;
		}
	}
		void state_bridgecross_0 ()
	{
		text.color = TextColor2;
		text.text = "You make it halfway across the bridge before the ropes snap " +
		"and the bridge collapses in a puff of dust. Maybe next time take note of rotten ropes? \n\n" +
		"Press R to restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
		void state_cliffcross_0 ()
	{
		text.color = TextColor3;
		text.text = "You begin to edge across the cliff. Halfway across the ledge, the stone began to crumble behind you. " +
		"You have only two options: Either sprint along the very fragile, thin ledge, or use superpowers. \n\n" +
		"Press A to run like wild, or press D to go web-slinging";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.exitgate_0;
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.marvel_0;
		}
	}
		void state_marvel_0 ()
	{
		text.color = TextColor1;
		text.text = "You swing through the air like a majestic copyrighted superhero... um. " +
		"Actually, scratch that, you died a horrible death from copyright infringement! \n\n" +
		"Press R to avoid lawsuit";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
		void state_exitgate_0 ()
	{
		text.color = TextColor3;
		text.text = "Running fast never fails! You arrive on the platform in front of the gate. " +
		"A massive locked gate is all that stands between you and your freedom. \n\n" +
		"Press A to kiss the sweet ground, or press D find a way to escape";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.lazyescape_0;
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.noescape;
			myEvent.Invoke();
		}
	}
		void state_lazyescape_0 ()
	{
		text.color = TextColor1;
		text.text = "You flop onto the ground and land on a tripwire, which causes a giant stone hammer to swing down from " +
		"the ceiling and toward your head. Fortunately, you are on the ground, and the hammer instead smashes the gate. \n\n" +
		"Press A to get up and escape, or press D kiss the ground again";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.lazydeath_0;
			myEvent.Invoke();
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.lazyescape_1;
		}
	}
		void state_lazydeath_0 ()
	{
		text.color = TextColor2;
		text.text = "You leap up and run through the gates. You forgot that the hammer would probably " +
		"swing back. Well, it did. And you died. \n\n" +
		"Press R to restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
		void state_noescape ()
	{
		text.color = TextColor2;
		text.text = "You search and search, but eventually realize that there was no way to escape. " +
		"Depressed and exhausted, you sit down against the wall and die. Lame. \n\n" +
		"Press R to restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}

	void state_lazyescape_1 ()
	{
		text.color = TextColor;
		text.text = "As you kiss the ground, the hammer swings back, and then fall into the pit. " +
		"You leap up quickly, and rush towards the stairwell. " +
		"Unfourtuately, the stairs don't lead to the dungeon exit." +
		"Rather, they spiral up into a second floor of the dungeon. Looks like this isn't over yet... \n\n" +
		"Press W";
		if (Input.GetKeyDown (KeyCode.W)) {
			myState = States.cell;
			myEventShift.Invoke();
		}
	}
		void state_hacker_0 ()
	{
		text.color = TextColor;
		text.text = "ERROR 37002763277623. " +
		"Key S has been pressed. Rerouting state..." +
		"Congratulations! You got the \n\n" +
		"Hack3r 3nd1ng";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
		void state_marvel_1 ()
	{
		text.color = TextColor;
		text.text = "You faced the lawsuit head on! With the power of Phoenix Wrong, " +
		"Second Best Attorney, you won! (you may have also sold your soul to a demon, to revive you from the dead, but...) " +
		"You won the lawsuit, and earned a bunch of money and also the \n\n" +
		"Fear no Courtroom Ending!";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke ();
		}
	}
			void state_wallcross_0 ()
	{
		text.color = TextColor3;
		text.text = "You stare intently at the walls. After a few minutes of intense concentration,  " +
		"you realized that there were, in fact, vines growing on the walls. the vines were low enough to reach, " +
		"so you could concieveably climb them across the pit. \n\n" +
		"Press A to climb the vines, or press D to try to climb the wall itself.";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.walldeath_0;
			myEvent.Invoke();
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.flood_0;
		}
	}
			void state_flood_0 ()
	{
		text.color = TextColor3;
		text.text = "You grab ahold of the bricks in the wall, and begin to climb.  " +
		"you make it two feet off the ground before the bricks in your hands give way, and water starts gushing though " +
		"the gaps. Soon the entire wall is threatening to give way. \n\n" +
		"Press A to cross the rope bridge, or press D to call upon your alien ancestors to save you from the rising tide.";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.flood_1;
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.electrickery;
			myEvent.Invoke();
		}
	}
		void state_electrickery ()
	{
		text.color = TextColor2;
		text.text = "You summon your alien ancestors, calling for them to arrive in their spacecrafts and rescue you from  " +
		"the waters. well, they certainly arrived in the spacecraft, seeing as how they crashlanded through four layers of stone and crushed you with the ship. \n\n" +
		"Press R to restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
		void state_flood_1 ()
	{
		text.color = TextColor3;
		text.text = "You leap onto the bridge, and the ropes snap. however, the rising water saves you from your grim demise, " +
		"and the broken wood and rope seem to naturally form a raft. you float to the other side of the chasm, and climb out in front of the gates. \n\n" +
		"Press A to look for a way out, or press D to kiss the ground.";
		if (Input.GetKeyDown (KeyCode.A)) {
			myState = States.noescape;
			myEvent.Invoke ();
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			myState = States.lazyescape_0;
		}
	}
		void state_walldeath_0 ()
	{
		text.color = TextColor2;
		text.text = "You grab onto the vines and begin to climb. halfway across the chasm, a vine that you kicked loose fell into a torch.  " +
		"the vines went up in flames faster than a match to gasoline, and you fell screaming into the darkness below. (sad trumpet noise plays). \n\n" +
		"Press R to restart";
		if (Input.GetKeyDown (KeyCode.R)) {
			myState = States.cell;
			myEvent2.Invoke();
		}
	}
}