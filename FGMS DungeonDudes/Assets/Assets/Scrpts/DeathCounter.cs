﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathCounter : MonoBehaviour {

	public Text Counter;
	private int Death = 0;

	// Use this for initialization
	void Start () {
	Death = 0;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void AddDeath ()
	{
	Death = Death +1;
	string DeathNumber = Death.ToString();
	Counter.text = "Death: " + DeathNumber;
	}
}
